from PIL import Image

def sheetToImages(img, length, n, m):
	arr = []
	for i in range(length):
		arr.append(img.crop((i * n,0,(i + 1) * n,m)))
	return arr

def imgToArr(img, n, m):
	arr = [0] * m * n
	for y in range(m):
		for x in range(n):
			gp = img.getpixel((x,y));
			arr[y*n+x] = (gp[0] + gp[1] + gp[2])/3
	return arr

def sheetToArr(img, length, n, m):
	imgs = sheetToImages(img,length,n,m)
	arr = [0] * length
	for i in range(length):
		arr[i] = imgToArr(imgs[i],n,m)
	return arr