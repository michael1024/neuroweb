import numpy as np

width = 100
height = 100

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 
	    'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	    'w', 'x', 'y', 'z']


raw_data = dict()

for l in alphabet:
	file = open(l + ".pmb")
	image = file.read()
	raw_data[l] = [[0] * width] * height

	extra_lines_counter = 0
	pixel_counter = 0
	for i in range(len(image)):
		#the first three lines are extra
		if extra_lines_counter < 3:
			if image[i] == '\n':
				extra_lines_counter += 1
		elif image[i] != '\n':
			imline = int(pixel_counter / width)
			imcol = pixel_counter - imline * width
			raw_data[l][imline][imcol] = int(image[i])

			pixel_counter += 1

##########
data = []
for letter, image in raw_data.items():
	data.append((image, letter))
