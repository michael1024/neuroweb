import random
import math
import abcImg

m = 10
n = 10
alphabet_length = 26
img = abcImg.Image.open("alphabet.png")
data = abcImg.sheetToArr(img, alphabet_length, n, m)


for im in range(len(data)):
    for p in range(len(data[im])):
        data[im][p] = data[im][p] / 255

########################################################

def sigmoid(x):
    return 1 / (1 + math.exp(-x))

def sigmoid_derivate(x):
    return math.exp(-x) / pow((math.exp(-x) + 1), 2)

def result(w, inp):
    """Return result of neuroweb with weights w on input inp."""
    num_of_pixels = len(w)
    alph_length = len(w[0])

    neurons_results = []
    sum_results = []
    for j in range(alph_length):
        sum_results.append(0)
        for i in range(num_of_pixels):
            sum_results[j] += w[i][j] * inp[i]

        neurons_results.append( sigmoid(sum_results[j]) )

    return (sum_results, neurons_results)

def gradient_descent(w, inp, real_result, coeff):
    """w_ij -= grad_of_mistake_ij * coeff"""
    alphabet_length = len(w[0])
    num_of_pixels = len(inp) 

    neuroweb_result = result(w, inp)
    sum_results = neuroweb_result[0]
    neurons_results = neuroweb_result[1] 

    grad = [] # grad[i][j] is partial derivate of mistake by w_ij
    #initializing
    for i in range(num_of_pixels):
        grad.append([])
        for j in range(alphabet_length):
            grad[i].append(0)


    for j in range(alphabet_length):
        common_factor = (neurons_results[j] - real_result[j]) * sigmoid_derivate(sum_results[j])
        for i in range(num_of_pixels):
            grad[i][j] = common_factor * inp[i]

    for j in range(alphabet_length):
        for i in range(num_of_pixels):
            w[i][j] -= coeff * grad[i][j]


#initializing of weights
w = []
for i in range(m * n):
    w.append([])
    for j in range(alphabet_length):
        w[i].append(random.random() * 0.0001)

def learn(w, data, alphabet_length, descent_speed):
    i = 0
    test_output_frequency = 10
    while(1):
        if i%test_output_frequency == 0:
            print("#########################################")
            print(i, ":")
            ## pass ##

        max_mistake = 0
        for im in range(len(data)):
            real_result = [0] * alphabet_length
            real_result[im] = 1

            gradient_descent(w, data[im], real_result, descent_speed)
            if i%test_output_frequency == 0:
                ## pass ##
                print(im, pow((max(result(w, data[im])[1]) - 1), 2), end="; ")
        
        i += 1
